import express, { json } from "express";
import { firstRouter } from "./src/Routes/firstRouter.js";
import vehicle from "./src/Routes/vehicleRouter.js";
import connectToMongoDb from "./src/connectMongoDB.js";
import teacherRouter from "./src/Routes/teacherRouter.js";
import bookRouter from "./src/Routes/bookRouter.js";
import collegeRouter from "./src/Routes/collegeRouter.js";
import studentRouter from "./src/Routes/studentRouter.js";
import departmentRouter from "./src/Routes/departmentRouter.js";
import classRoomRouter from "./src/Routes/classRoomRouter.js";
import traineeRouter from "./src/Routes/traineeRouter.js";
import jwt from "jsonwebtoken";
import { secretKey } from "./src/constant.js";
import { fileRouter } from "./src/Routes/fileRouter.js";

const app = express();
app.use(json());

const port = 8000;

app.use(firstRouter);
app.use("/vehicle", vehicle);
app.use("/teacher", teacherRouter);
app.use("/book", bookRouter);
app.use("/college", collegeRouter);
app.use("/student", studentRouter);
app.use("/department", departmentRouter);
app.use("/classroom", classRoomRouter);
app.use("/trainee", traineeRouter);
app.use("/file", fileRouter);
app.use(express.static("./public"));

app.listen(8000, () => {
  console.log(`Application is running at ${port} successfully`);
});

connectToMongoDb();

// let infoObj = {
//   _id: "87678686868",
// };

// let expiryInfo = {
//   expiresIn: "2d",
// };
// let token = jwt.sign(infoObj, secretKey, expiryInfo);

// console.log(token);

// let token =
//   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI4NzY3ODY4Njg2OCIsImlhdCI6MTY5NjMxNjM1NywiZXhwIjoxNjk2NDg5MTU3fQ.jQvuYfA-dCDOOowHQmMvolBZw3by2tM_A8HXZOiBKfI";

// try {
//   let infoObj = jwt.verify(token, secretKey);
//   console.log(infoObj);
// } catch (error) {
//   console.log(error.message);
// }
