type: module helps to support import export
console comes into terminal in backend
When JSON is utilized, the line that comes after it can handle JSON data processing.
Always write dynamic routes in the last
for one request, we must have one response.

It is a function which has req, res and next. next() is used to trigger another middleware
controller is a middleware which is used at last
MIDDLEWARES BASED ON ERROR
there are two middlewares based on error >------<> normal and error
normal middleware is defined using (req,res,next)=>{} and triggered by next().
Error middleware is defined using (err,req,res,next)=>{} and trigerred by next(value)

MIDDLEWARES BASED ON IT'S LOCATION
There are three types of middlware - application-level middleware and route-level middleware.

SEARCHING
type doesnt matter in searching, only text does.
to search greater than in searching. we have to do :-
greater than: use gt
find({age:{$gt:20}})

greater or equal: use gte
find({age:{$gte:20}})

less than: use lt
find({
    age:{$lt:20}
})

less or equal: use $lte
find({
    age:{$lte:20}
})

not equal: use $ne
find({age:{$ne:20}})

includes: use $in
find({age:{$in:[22,26,27]}})

BOTH CONDITION MEET: use $and
find({$and:[
    {name:"nikesh"},
    {age:21}
    ]
})

find({$and:[
    {age:{$gte:25}},
    {age:{$lte:30}}
    ]
})

EITHER ONE: use $or
find({$or:[
    {name: "sandip"},
    {name: "chhimi"},
    {age: 22},
    {age: 16},
    ]
})

REGEX SEARCHING
its not the exact searching, but not exact one. More like includes. it is denoted by /text/
find(name:/text/)
if it is denoted as /text/i, then it becomes text insensitive.
find(name:/text/i)
if it is denoted as /^text/, it means it starts with text
find(name:/^text/)
if it is denoted as /text$/, it means it ends with text
find(name:/text$/)

FIND AND SELECT.
FIND has control over the object wheras SELECT has control over the object property
the order goes like this. find, sort, select, skip, limit


