import { model } from "mongoose";
import bookSchema from "./bookSchema.js";
import teacherSchema from "./teacherSchema.js";
import studentSchema from "./studentSchema.js";
import collegeSchema from "./collegeSchema.js";
import classRoomSchema from "./classroomSchema.js";
import departmentSchema from "./departmentSchema.js";
import detailsSchema from "./detailSchema.js";
import traineeSchema from "./traineeSchema.js";

export let Detail = model("Detail", detailsSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Book = model("Book", bookSchema);
export let Student = model("Student", studentSchema);
export let College = model("College", collegeSchema);
export let ClassRoom = model("ClassRoom", classRoomSchema);
export let Department = model("Department", departmentSchema);
export let Trainee = model("Trainee", traineeSchema);
