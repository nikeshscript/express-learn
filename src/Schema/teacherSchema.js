import { Schema } from "mongoose";
// schema//obj defining
let teacherSchema = Schema({
  name: {
    type: String,
    required: [true, "Please enter the Name"],
  },
  age: {
    type: Number, //number is a datatype in javascript
    required: [true, "please enter the age"],
  },
  subject: {
    type: String,
    required: [true, "Please enter the Name"],
  },

  isMarried: {
    type: Boolean,
    required: true,
  },
});
export default teacherSchema;
