import { Schema } from "mongoose";
// schema//obj defining
let classRoomSchema = Schema({
  name: {
    type: String,
    required: [true, "Please enter the Name"],
  },
  numberofBench: {
    type: Number, //number is a datatype in javascript
    required: [true, "please enter the bench"],
  },
  hasTv: {
    type: Boolean,
    required: [true, "Please enter the Name"],
  },
});
export default classRoomSchema;
