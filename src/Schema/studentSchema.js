import { Schema } from "mongoose";
// schema//obj defining
let studentSchema = Schema({
  name: {
    type: String,
    required: [true, "Please enter the Name"],
  },
  class: {
    type: Number, //number is a datatype in javascript
    required: [true, "please enter the age"],
  },
  faculty: {
    type: String,
    required: [true, "Please enter the Name"],
  },
});
export default studentSchema;
