import { Schema } from "mongoose";
// schema//obj defining
let detailsSchema = Schema({
  name: {
    type: String,
    required: [true, "Please enter the Name"],
  },
  age: {
    type: Number, //number is a datatype in javascript
    required: [true, "please enter the age"],
  },
  isMarried: {
    type: Boolean,
    required: true,
  },
});
export default detailsSchema;
