//book name, auther ,price, isAvailable

import { Schema } from "mongoose";

let bookSchema = Schema({
  name: {
    type: String,
    required: [true, "Please enter the name"],
  },
  arthur: {
    type: String,
    required: [true, "Please enter the arthur"],
  },
  price: {
    type: Number,
    required: [true, "please add a price"],
  },
  isAvailable: {
    type: Boolean,
    required: [true, "please select the availability"],
  },
});
export default bookSchema;
