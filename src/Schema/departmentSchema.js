import { Schema } from "mongoose";
// schema//obj defining
let departmentSchema = Schema({
  name: {
    type: String,
    required: [true, "Please enter the Name"],
  },
  totalmember: {
    type: Number, //number is a datatype in javascript
    required: [true, "please enter the age"],
  },
  hod: {
    type: String,
    required: [true, "Please enter the Name"],
  },
});
export default departmentSchema;
