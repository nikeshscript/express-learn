import { Trainee } from "../Schema/model.js";

export let createTrainee = async (req, res) => {
  let data = req.body;
  try {
    let result = await Trainee.create(data);
    res.json({
      success: true,
      message: "Successfully added",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readAllTrainee = async (req, res) => {
  try {
    //ORDER => find ---> sort ---> select ---> skip ---> limit
    // let result = await Trainee.find();
    let result = await Trainee.find().sort("age name").select("name age -_id");
    // let result = await Trainee.find({ name: /nikesh/i }).select("name age -_id");
    // let result = await Trainee.find().select("name age -_id").limit("2");
    // let result = await Trainee.find().select("name age -_id").skip("2");
    res.json({
      success: true,
      message: "Trainee fetched successfully",
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: "Data failed to fetch",
    });
  }
};

export let readTraineeById = async (req, res) => {
  const id = req.params.id;
  try {
    const result = await Trainee.findById(id);
    res.json({
      success: true,
      message: `${result.name} fetched success`,
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateTrainee = async (req, res) => {
  const id = req.params.id;
  const data = req.body;
  try {
    const result = await Trainee.findByIdAndUpdate(id, data, { new: true });
    res.json({
      success: true,
      message: `${data.name} updated success`,
      data: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteTrainee = async (req, res) => {
  const id = req.params.id;
  try {
    const result = await Trainee.findByIdAndDelete(id);
    res.json({
      success: true,
      message: "Trainee deleted successfully",
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
