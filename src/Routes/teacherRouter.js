import { Router } from "express";
import { Teacher } from "../Schema/model.js";

let teacherRouter = Router();
teacherRouter
  .route("/")
  .post(async (req, res) => {
    let data = req.body;
    try {
      let result = await Teacher.create(data);
      res.json({
        success: true,
        message: " teacher post sucessfully",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res) => {
    try {
      let result = await Teacher.find({});
      res.json({
        success: true,
        message: " teacher post sucessfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

teacherRouter
  .route("/:id")
  .get(async (req, res) => {
    let id = req.params.id;
    try {
      let result = await Teacher.findById(id);
      res.json({
        success: true,
        message: "Teacher get success",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .patch(async (req, res) => {
    let id = req.params.id;
    let data = req.body;
    try {
      let result = await Teacher.findByIdAndUpdate(id, data, { new: true });
      res.json({
        success: true,
        message: "Teacher update success",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: "Teacher update failed",
      });
    }
  })
  .delete(async (req, res) => {
    let id = req.params.id;
    try {
      let result = await Teacher.findByIdAndDelete(id);
      res.json({
        success: true,
        message: "Teacher deleted success",
      });
    } catch (error) {
      res.json({
        success: false,
        message: "Teacher deletion failed",
      });
    }
  });
//   .patch((req, res) => {
//     console.log(req.body);
//     res.json({ success: true, message: "patch sucessfully", data: req.body });
//   })
//   .delete((req, res) => {
//     res.json({ success: true, message: "delete sucessfully" });
//   });
export default teacherRouter;
