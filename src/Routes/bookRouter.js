import { Router } from "express";
import { Book } from "../Schema/model.js";

let bookRouter = Router();

bookRouter
  .route("/")
  .post(async (req, res) => {
    let data = req.body;
    try {
      let result = await Book.create(data);
      res.json({
        success: true,
        message: "Successfully added",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res) => {
    try {
      let result = await Book.find();
      res.json({
        success: true,
        message: "Book fetched successfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: "Data failed to fetch",
      });
    }
  });

bookRouter
  .route("/:id")
  .get(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await Book.findById(id);
      res.json({
        success: true,
        message: `${result.name} fetched success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .patch(async (req, res) => {
    const id = req.params.id;
    const data = req.body;
    try {
      const result = await Book.findByIdAndUpdate(id, data, { new: true });
      res.json({
        success: true,
        message: `${data.name} updated success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .delete(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await Book.findByIdAndDelete(id);
      res.json({
        success: true,
        message: "Book deleted successfully",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

export default bookRouter;
