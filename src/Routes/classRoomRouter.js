import { Router } from "express";
import { ClassRoom } from "../Schema/model.js";

let classRoomRouter = Router();

classRoomRouter
  .route("/")
  .post(async (req, res) => {
    let data = req.body;
    try {
      let result = await ClassRoom.create(data);
      res.json({
        success: true,
        message: "Successfully added",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res) => {
    try {
      let result = await ClassRoom.find();
      res.json({
        success: true,
        message: "classRoom fetched successfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: "Data failed to fetch",
      });
    }
  });

classRoomRouter
  .route("/:id")
  .get(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await ClassRoom.findById(id);
      res.json({
        success: true,
        message: `${result.name} fetched success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .patch(async (req, res) => {
    const id = req.params.id;
    const data = req.body;
    try {
      const result = await ClassRoom.findByIdAndUpdate(id, data, { new: true });
      res.json({
        success: true,
        message: `${data.name} updated success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .delete(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await ClassRoom.findByIdAndDelete(id);
      res.json({
        success: true,
        message: "classRoom deleted successfully",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

export default classRoomRouter;
