import { Router } from "express";
import { College } from "../Schema/model.js";

let collegeRouter = Router();

collegeRouter
  .route("/")
  .post(async (req, res) => {
    let data = req.body;
    try {
      let result = await College.create(data);
      res.json({
        success: true,
        message: "Successfully added",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res) => {
    try {
      let result = await College.find();
      res.json({
        success: true,
        message: "college fetched successfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: "Data failed to fetch",
      });
    }
  });

collegeRouter
  .route("/:id")
  .get(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await College.findById(id);
      res.json({
        success: true,
        message: `${result.name} fetched success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .patch(async (req, res) => {
    const id = req.params.id;
    const data = req.body;
    try {
      const result = await College.findByIdAndUpdate(id, data, { new: true });
      res.json({
        success: true,
        message: `${data.name} updated success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .delete(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await College.findByIdAndDelete(id);
      res.json({
        success: true,
        message: "college deleted successfully",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

export default collegeRouter;
