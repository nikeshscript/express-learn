import { Router } from "express";
import { Student } from "../Schema/model.js";

let studentRouter = Router();

studentRouter
  .route("/")
  .post(async (req, res) => {
    let data = req.body;
    try {
      let result = await Student.create(data);
      res.json({
        success: true,
        message: "Successfully added",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res) => {
    try {
      let result = await Student.find();
      res.json({
        success: true,
        message: "student fetched successfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: "Data failed to fetch",
      });
    }
  });

studentRouter
  .route("/:id")
  .get(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await Student.findById(id);
      res.json({
        success: true,
        message: `${result.name} fetched success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .patch(async (req, res) => {
    const id = req.params.id;
    const data = req.body;
    try {
      const result = await Student.findByIdAndUpdate(id, data, { new: true });
      res.json({
        success: true,
        message: `${data.name} updated success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .delete(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await Student.findByIdAndDelete(id);
      res.json({
        success: true,
        message: "student deleted successfully",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

export default studentRouter;
