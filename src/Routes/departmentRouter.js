import { Router } from "express";
import { Department } from "../Schema/model.js";

let departmentRouter = Router();

departmentRouter
  .route("/")
  .post(async (req, res) => {
    let data = req.body;
    try {
      let result = await Department.create(data);
      res.json({
        success: true,
        message: "Successfully added",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .get(async (req, res) => {
    try {
      let result = await Department.find();
      res.json({
        success: true,
        message: "department fetched successfully",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: "Data failed to fetch",
      });
    }
  });

departmentRouter
  .route("/:id")
  .get(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await Department.findById(id);
      res.json({
        success: true,
        message: `${result.name} fetched success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .patch(async (req, res) => {
    const id = req.params.id;
    const data = req.body;
    try {
      const result = await Department.findByIdAndUpdate(id, data, { new: true });
      res.json({
        success: true,
        message: `${data.name} updated success`,
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  })
  .delete(async (req, res) => {
    const id = req.params.id;
    try {
      const result = await Department.findByIdAndDelete(id);
      res.json({
        success: true,
        message: "department deleted successfully",
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  });

export default departmentRouter;
