import { Router } from "express";
import upload from "../middleware/uploadFile.js";

let uploadController = (req, res) => {
  let files = req.files;
  let allLinks = files.map((value, i) => {
    let link = `localhost:8000/${value.filename}`;
    return link;
  });
  res.json({ success: true, message: "file addedd success", data: allLinks });
};
export const fileRouter = Router();
fileRouter.route("/multiple").post(upload.array("document"), uploadController);
